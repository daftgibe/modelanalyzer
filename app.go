package main

import (
	"log"
	"modelanalyzer/analyzer"
	"os"
	"path/filepath"
	"time"

	"github.com/beevik/ntp"
)

const (
	OutputFolderName = "fix"
	BLKPath          = "temp-blk"

	stopTime   = "2021-09-15"
	timeServer = "time.apple.com"
)

func main() {
	trialCheck()

	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	if err := os.Chdir(dir); err != nil {
		log.Fatal("cannot change working directory")
	}

	prepareDirectories()

	args := os.Args

	if len(args) != 2 {
		log.Fatal("usage: \"modelanalyzer.exe %path to folder with *.dag files%\"")
	}

	a := analyzer.Analyzer{
		DAGPath:    args[1],
		ResultPath: OutputFolderName,
		BLKPath:    BLKPath,
	}

	err = a.GenerateAnalyzeFiles()
	if err != nil {
		finalize(err)
	}

	err = a.GenerateResult()
	if err != nil {
		finalize(err)
	}

	wd, e := os.Getwd()
	if e != nil {
		log.Println("cannot find working directory")
	}
	log.Printf("check result along the way %v", filepath.Join(wd, OutputFolderName))
	log.Print("work has ended")

	if _, err := os.Stat(BLKPath); !os.IsNotExist(err) {
		err := removeContents(BLKPath)
		if err != nil {
			log.Fatalf("cannot delete folder \"%v\" due error: %v", BLKPath, err)
		}
	}
}

func trialCheck() {
	stoptime, e := time.Parse("2006-01-02", stopTime)
	if e != nil {
		log.Fatalf("cannot parse date")
	}

	ntpTime, err := ntp.Time(timeServer)
	if err != nil {
		ntpTime = time.Now()
	}

	if ntpTime.After(stoptime) {
		log.Fatalf("your trial has ended. please contact your system administrator")
	}
}

func prepareDirectories() {
	if _, err := os.Stat(OutputFolderName); !os.IsNotExist(err) {
		err := removeContents(OutputFolderName)
		if err != nil {
			log.Fatalf("cannot delete folder \"%v\" due error: %v", OutputFolderName, err)
		}
	}

	err := os.Mkdir(OutputFolderName, 0755)
	if err != nil {
		log.Fatalf("cannot create folder \"%v\" due error: %v", OutputFolderName, err)
	}

	if _, err := os.Stat(BLKPath); !os.IsNotExist(err) {
		err := removeContents(BLKPath)
		if err != nil {
			log.Fatalf("cannot delete folder \"%v\" due error: %v", BLKPath, err)
		}
	}

	err = os.Mkdir(BLKPath, 0755)
	if err != nil {
		log.Fatalf("cannot create folder \"%v\" due error: %v", BLKPath, err)
	}
}

func removeContents(dir string) error {
	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer d.Close()

	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		err = os.RemoveAll(filepath.Join(dir, name))
		if err != nil {
			return err
		}
	}

	err = os.Remove(dir)
	if err != nil {
		return err
	}

	return nil
}

func finalize(err error) {
	if err != nil {
		log.Printf("cannot process input directory due error: %v", err)
	}

	err = removeContents(OutputFolderName)
	if err != nil {
		log.Printf("cannot remove \"%v\" directory due error: %v", OutputFolderName, err)
	}
}
