package analyzer

import (
	"errors"
	"strings"
)

var DefaultNames = []string{"Cylinder", "Box", "Plane", "Sphere", "Object"}

type Node struct {
	fileName string
	lines    []Line
}

func (n *Node) Check() []error {
	errs := make([]error, 0)

	if err := n.DefaultNameCheck(); err != nil {
		errs = append(errs, err)
	}
	if err := n.ChannelsCheck(); err != nil {
		errs = append(errs, err)
	}

	return errs
}

func (n *Node) DefaultNameCheck() error {
	for i := range n.lines {
		splitted := strings.Split(string(n.lines[i]), ":")
		paramName := strings.ReplaceAll(splitted[0], " ", "")
		if paramName == "name" {
			val := strings.Trim(strings.Replace(splitted[1], "t=", "", -1), "\"")
			for ii := range DefaultNames {
				if strings.HasPrefix(val, DefaultNames[ii]) {
					return errors.New("Данный меш создержит стандартное имя, нужно дать ему логичное название для более удобной работы")
				}
			}
		}
	}

	return nil
}

func (n *Node) ChannelsCheck() error {
	for i := range n.lines {
		l := strings.ReplaceAll(string(n.lines[i]), " ", "")
		if strings.HasPrefix(l, "obj:") {
			if strings.Contains(l, "channels") {
				if !strings.Contains(l, "1channels") && !strings.Contains(l, "0channels") {
					return errors.New("В данном меше больше 1 UV канала. Если на него не назначен шейдер rendinst_flag_colored или rendinst_vcolor_layered, все лишние каналы удалить, включая vertex color, если они не используются")
				}
			}
		}
	}

	return nil
}
