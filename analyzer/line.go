package analyzer

import (
	"errors"
	"strings"
)

type Line struct {
	Raw      string
	Number   int
	LineType LineType
}

type LineType string

const (
	Name          LineType = "name"
	Class         LineType = "class"
	Tex16support  LineType = "tex16support"
	Twosided      LineType = "twosided"
	AMB           LineType = "amb"
	Diff          LineType = "diff"
	Spec          LineType = "spec"
	Emis          LineType = "emis"
	Power         LineType = "power"
	Script        LineType = "script"
	Tex           LineType = "tex"
	MaterialStart LineType = "material {"
	MaterialEnd   LineType = "}"
	Empty         LineType = ""
)

func NewLine(l string) Line {
	switch l {
	case MaterialStart:
		return Line{
			Raw:      l,
			Number:   0,
			LineType: MaterialStart,
		}
	}
	if l == string(MaterialStart) {

	}
}

func (l *Line) Check() error {
	ps := strings.ReplaceAll(string(*l), " ", "")

	sps := strings.Split(ps, ":")

	switch {
	case sps[0] == string(Name):
		n := sps[1]
		n = strings.Trim(n, "t=\"")
		n = strings.Trim(n, "\"")
		if n == "" {
			return errors.New("name не может быть пустым")
		}
		if strings.Contains(n, "Material#") {
			return errors.New("Данный материал содержит стандартное имя, нужно дать ему логичное название для более удобной работы")
		}
	case sps[0] == string(Tex16support):
		t := sps[1]
		t = strings.Trim(t, "b=")
		if t != "no" && t != "yes" {
			return errors.New("tex16support имеет невалидное значение")
		}
	case sps[0] == string(Twosided):
		t := sps[1]
		t = strings.Trim(t, "i=")
		if t != "0" && t != "1" {
			return errors.New("twosided имеет невалидное значение")
		}
	case sps[0] == string(AMB):
		t := sps[1]
		t = strings.Trim(t, "ip3=")
		if t == "" {
			return errors.New("amb имеет невалидное значение")
		}
	case sps[0] == string(Diff):
		t := sps[1]
		t = strings.Trim(t, "ip3=")
		if t == "" {
			return errors.New("diff имеет невалидное значение")
		}
	case sps[0] == string(Spec):
		t := sps[1]
		t = strings.Trim(t, "ip3=")
		if t == "" {
			return errors.New("spec имеет невалидное значение")
		}
	case sps[0] == string(Emis):
		t := sps[1]
		t = strings.Trim(t, "ip3=")
		if t == "" {
			return errors.New("emis имеет невалидное значение")
		}
	case sps[0] == string(Power):
		t := sps[1]
		t = strings.Trim(t, "r=")
		if t == "" {
			return errors.New("power имеет невалидное значение")
		}
	case sps[0] == string(Script):
		t := sps[1]
		t = strings.Trim(t, "t=\"")
		t = strings.Trim(t, "\"")
		if t == "" {
			return errors.New("script имеет невалидное значение")
		}

		ts := strings.Split(t, "=")
		numbers := ts[1]
		if strings.Contains(numbers, " ") {
			return errors.New("Необходимо удалить все пробелы в числовых значениях добавленых параметров")
		}
		tsc := strings.Split(numbers, ",")
		if len(tsc) != 1 && len(tsc) != 4 {
			return errors.New("Проверь, параметры материалов, возможно указано неверное количество значений, либо имеется лишняя/пропущенная запятая")
		}
	case strings.HasPrefix(sps[0], "tex"):
		s := sps[1:]
		ss := strings.Join(s, " ")
		ss = strings.Trim(ss, "t=\"")
		ss = strings.Trim(ss, "\"")
		if ss == "" {
			return errors.New("tex имеет невалидное значение")
		}
	}
	return nil
}

func (l *Line) Type() Line {
	ps := strings.ReplaceAll(string(*l), " ", "")

	sps := strings.Split(ps, ":")

	switch {
	case sps[0] == string(Name):
		return Name
	case sps[0] == string(Class):
		return Class
	case sps[0] == string(Tex16support):
		return Tex16support
	case sps[0] == string(Twosided):
		return Twosided
	case sps[0] == string(AMB):
		return AMB
	case sps[0] == string(Diff):
		return Diff
	case sps[0] == string(Spec):
		return Spec
	case sps[0] == string(Emis):
		return Emis
	case sps[0] == string(Power):
		return Power
	case sps[0] == string(Script):
		return Script
	case strings.HasPrefix(sps[0], "tex"):
		return Tex
	default:
		return Unknown
	}
}
