package analyzer

import (
	"bufio"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

type Analyzer struct {
	DAGPath string
	BLKPath string

	ResultPath string
}

func (a *Analyzer) GenerateAnalyzeFiles() error {
	return filepath.Walk(a.DAGPath,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			_, fileNameWithExt := filepath.Split(path)
			if filepath.Ext(fileNameWithExt) != ".dag" {
				return nil
			}

			fileName := strings.TrimSuffix(fileNameWithExt, filepath.Ext(fileNameWithExt))

			blkOutput := filepath.Join(a.BLKPath, fileName+".blk")
			cmdMat := exec.Command("mat_remap-dev", "-q", path, blkOutput)
			if err = cmdMat.Run(); err != nil {
				log.Printf("cannot process \"%v\" path due error: %v", path, err)
			}

			dagtmpOutput := filepath.Join(a.BLKPath, fileName+".dagtmp")
			cmdDag := exec.Command("dag_script-dev", "-e", path, dagtmpOutput)
			if err := cmdDag.Run(); err != nil {
				log.Printf("cannot process \"%v\" path due error: %v", path, err)
			}

			return nil
		})
}

func (a *Analyzer) GenerateResult() error {
	return filepath.Walk(a.BLKPath,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			_, fileNameWithExt := filepath.Split(path)
			if filepath.Ext(fileNameWithExt) != ".blk" {
				return nil
			}

			fileName := strings.TrimSuffix(fileNameWithExt, filepath.Ext(fileNameWithExt))
			errorInfoOutput := filepath.Join(a.ResultPath, fileName+".errorinfo")

			blkAnalyzer := BLKAnalyzer{
				BLKPath:    path,
			}
			errs := blkAnalyzer.Analyze()

			dagtmpPath := strings.Replace(path, ".blk", ".dagtmp", 1)
			dagtmpAnalyzer := DAGTMPAnalyzer{
				DAGTMPPath: dagtmpPath,
			}
			dagtmpErrs := dagtmpAnalyzer.Analyze()
			for k, v := range dagtmpErrs {
				if _, ok := errs[k]; !ok {
					errs[k] = v
				} else {
					for i := range v {
						errs[k] = append(errs[k], v[i])
					}
				}
			}

			a.writeResult(errorInfoOutput, errs)

			return nil
		})
}

func (a *Analyzer) writeResult(outputPath string, materials map[string][]string) {
	if len(materials) == 0 {
		return
	}

	outputf, err := os.Create(outputPath)
	if err != nil {
		log.Printf("cannot create \"%v\" file due error: %v", outputPath, err)
	}
	defer outputf.Close()

	outputw := bufio.NewWriter(outputf)
	defer outputw.Flush()

	for k, v := range materials {
		_, e := outputw.WriteString(k)
		if e != nil {
			log.Printf("cannot write material into %v file", outputPath)
			if err := os.Remove(outputPath); err != nil {
				log.Printf("cannot remove \"%v\" file due error: %v", outputPath, err)
			}
			break
		}

		_, err = outputw.WriteString("\n")
		if err != nil {
			if err := os.Remove(outputPath); err != nil {
				log.Printf("cannot remove \"%v\" file due error: %v", outputPath, err)
			}
			break
		}

		em := make(map[string]struct{})
		for ii := range v {
			if _, ok := em[v[ii]]; ok {
				continue
			}
			_, err := outputw.WriteString(v[ii] + "\n")
			if err != nil {
				log.Printf("cannot write error %v", v[ii])
				if err := os.Remove(outputPath); err != nil {
					log.Printf("cannot remove \"%v\" file due error: %v", outputPath, err)
				}
				break
			}
			em[v[ii]] = struct{}{}
		}
		_, err := outputw.WriteString("\n")
		if err != nil {
			if err := os.Remove(outputPath); err != nil {
				log.Printf("cannot remove \"%v\" file due error: %v", outputPath, err)
			}
			break
		}
	}
}
