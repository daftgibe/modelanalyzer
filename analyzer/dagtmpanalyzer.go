package analyzer

import (
	"bufio"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
)

const (
	NodeStart = "node{"
	NodeEnd   = "}"
)

type DAGTMPAnalyzer struct {
	DAGTMPPath string
}

func (d *DAGTMPAnalyzer) Analyze() map[string][]string {
	f, err := os.Open(d.DAGTMPPath)
	if err != nil {
		log.Printf("cannot open dagtmp \"%v\" file due error: %v", d.DAGTMPPath, err)
		return nil
	}
	defer f.Close()

	nodes := make([][]string, 0)
	nodeLines := make([]string, 0)

	r := bufio.NewReader(f)
	for {
		l, _, err := r.ReadLine()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Printf("cannot read line from dagtmp \"%v\" file due error: %v. stop process file", d.DAGTMPPath, err)
			return nil
		}

		switch s := string(l); s {
		case NodeStart:
			nodeLines = make([]string, 0)
			nodeLines = append(nodeLines, s)
		case NodeEnd:
			nodeLines = append(nodeLines, s)
			nodes = append(nodes, nodeLines)
		case EmptyLine:
			continue
		default:
			nodeLines = append(nodeLines, s)
		}
	}

	result := make(map[string][]string, 0)
	for i := range nodes {
		n := Node{
			fileName: strings.TrimSuffix(filepath.Base(d.DAGTMPPath), filepath.Ext(filepath.Base(d.DAGTMPPath))),
		}
		for ii := range nodes[i] {
			n.lines = append(n.lines, Line(nodes[i][ii]))
		}

		errs := n.Check()
		if len(errs) > 0 {
			nodeAsString := strings.Join(nodes[i], "\n")
			for _, e := range errs {
				if _, ok := result[nodeAsString]; !ok {
					result[nodeAsString] = []string{e.Error()}
				} else {
					result[nodeAsString] = append(result[nodeAsString], e.Error())
				}
			}
		}
	}

	return result
}
