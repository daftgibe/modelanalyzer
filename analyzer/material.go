package analyzer

import (
	"errors"
	"path/filepath"
	"strconv"
	"strings"
)

type Material struct {
	fileName string
	lines    []Line
}

func (m *Material) Check() []error {
	errs := make([]error, 0)

	class, name := "", ""
	for i := range m.lines {
		if m.lines[i].Type() == Class {
			class = strings.Trim(strings.Split(string(m.lines[i]), "=")[1], "\"")
		}
		if m.lines[i].Type() == Name {
			name = strings.Trim(strings.Split(string(m.lines[i]), "=")[1], "\"")
		}
	}

	if class == "" && name == "autoNamedMat_0" {
		return errs
	}


	if err := m.EmptyClassCheck(); err != nil {
		errs = append(errs, err)
		return errs
	}
	if err := m.AllLinesCheck(); err != nil {
		for e := range err {
			errs = append(errs, e)
		}
	}
	if err := m.ScriptMicroDetailsCheck(); err != nil {
		for e := range err {
			errs = append(errs, e)
		}
	}
	if err := m.DestrCheck(); err != nil {
		errs = append(errs, err)
	}
	if err := m.ScriptPaintDetailedCheck(); err != nil {
		for e := range err {
			errs = append(errs, e)
		}
	}
	if err := m.ScriptFourValuesCheck(); err != nil {
		errs = append(errs, err)
	}
	if err := m.ScriptPerlinLayerCheck(); err != nil {
		errs = append(errs, err)
	}
	if err := m.DiffuseNormalCheck(); err != nil {
		errs = append(errs, err)
	}
	if err := m.TexCheck(); err != nil {
		for e := range err {
			errs = append(errs, e)
		}
	}
	if err := m.MaskGammaCheck(); err != nil {
		errs = append(errs, err)
	}
	if err := m.UsePaintingCheck(); err != nil {
		errs = append(errs, err)
	}

	return errs
}

func (m *Material) AllLinesCheck() map[error]struct{} {
	errs := make(map[error]struct{}, 0)
	for i := range m.lines {
		e := m.lines[i].Check()
		if e == nil {
			continue
		}

		if _, ok := errs[e]; !ok {
			errs[e] = struct{}{}
		}
	}

	return errs
}

func (m *Material) DestrCheck() error {
	if !strings.HasSuffix(m.fileName, "_destr.lod00") {
		return nil
	}

	for _, l := range m.lines {
		if l.Type() == Class && strings.HasSuffix(string(l), "_decal\"") {
			return errors.New("В дестре не должо быть декалей. Декали из дестра нужно удалить")
		}
	}

	return nil
}

func (m *Material) ScriptMicroDetailsCheck() map[error]struct{} {
	var class Line
	scriptLines := make([]Line, 0)
	for _, l := range m.lines {
		if l.Type() == Script {
			scriptLines = append(scriptLines, l)
		}
		if l.Type() == Class {
			class = l
		}
	}

	errs := make(map[error]struct{}, 0)

	switch {
	case strings.Contains(string(class), "decal"), strings.Contains(string(class), "gi_black"):
		for _, l := range scriptLines {
			if strings.Contains(string(l), "micro_detail_layer") {
				errs[errors.New("micro_detail не должно быть")] = struct{}{}
			}
		}

		return nil
	default:
		if strings.HasSuffix(m.fileName, "lod00") && !strings.HasSuffix(m.fileName, "_destr.lod00") {
			mdlFound, mdluvFound := false, false
			for _, l := range scriptLines {
				if strings.Contains(string(l), "micro_detail_layer=") {
					mdlFound = true
				}
				if strings.Contains(string(l), "micro_detail_layer_uv_scale") {
					mdluvFound = true
				}
			}

			if !mdlFound {
				errs[errors.New("Отсутствуют параметры micro_detail_layer в lod00, проверить, пропущен ли он по ошибке")] = struct{}{}
			}
			if !mdluvFound {
				errs[errors.New("Отсутствуют параметры micro_detail_layer_uv_scale в lod00, проверить, пропущен ли он по ошибке")] = struct{}{}
			}
		} else {
			for _, l := range scriptLines {
				if strings.Contains(string(l), "micro_detail_layer") {
					errs[errors.New("Нужно удалить micro_detail")] = struct{}{}
				}
			}
		}
	}

	return errs
}

func (m *Material) ScriptPaintDetailedCheck() map[error]struct{} {
	scriptLines := make([]Line, 0)
	for _, l := range m.lines {
		if l.Type() == Script {
			scriptLines = append(scriptLines, l)
		}
	}

	paintDetailsFound, paletteIndexFound := false, false

	for _, l := range scriptLines {
		if strings.Contains(string(l), "paint_details") {
			paintDetailsFound = true
		}
		if strings.Contains(string(l), "palette_index") {
			paletteIndexFound = true
		}
	}

	errs := make(map[error]struct{})

	if paintDetailsFound && !paletteIndexFound {
		errs[errors.New("Нужно добавить palette_index")] = struct{}{}
	}
	if !paintDetailsFound && paletteIndexFound {
		errs[errors.New("Нужно добавить paint_details")] = struct{}{}
	}

	return errs
}

func (m *Material) ScriptFourValuesCheck() error {
	scriptLines := make([]Line, 0)
	for _, l := range m.lines {
		if l.Type() == Script {
			scriptLines = append(scriptLines, l)
		}
	}

	for _, l := range scriptLines {
		if err := l.Check(); err != nil {
			return err
		}
	}

	return nil
}

func (m *Material) MaskGammaCheck() error {
	var class Line
	scriptLines := make([]Line, 0)
	for _, l := range m.lines {
		if l.Type() == Script {
			scriptLines = append(scriptLines, l)
		}
		if l.Type() == Class {
			class = l
		}
	}

	if strings.Contains(string(class), "rendinst_perlin_layered") ||
		strings.Contains(string(class), "dynamic_perlin_layered") {
		for i := range scriptLines {
			if strings.Contains(string(scriptLines[i]), "mask_gamma") {
				mgla := strings.Split(string(scriptLines[i]), ":")
				mgl := strings.Trim(mgla[1], "\"")

				numbers := strings.Split(strings.Split(mgl, "=")[2], ",")
				for i := range numbers {
					if strings.Contains(numbers[i], " ") {
						return errors.New("Необходимо удалить все пробелы в числовых значениях добавленых параметров")
					}
				}

				for i := range numbers {
					f, e := strconv.ParseFloat(numbers[i], 64)
					if e != nil {
						return errors.New("Невозможно перевести в число " + numbers[i])
					}
					if f <= 0 {
						return errors.New("Использование в mask_gamma значений 0 и ниже, считается багом. Необходимо вписывать только положительные значения")
					}
				}
			}
		}
	}

	return nil
}

func (m *Material) ScriptPerlinLayerCheck() error {
	var class Line
	scriptLines := make([]Line, 0)
	for _, l := range m.lines {
		if l.Type() == Script {
			scriptLines = append(scriptLines, l)
		}
		if l.Type() == Class {
			class = l
		}
	}

	blendNormalsFound := false
	invertHeightsFound := false
	maskGammaFound := false
	paintDetailsFound := false

	if strings.Contains(string(class), "rendinst_perlin_layered") ||
		strings.Contains(string(class), "dynamic_perlin_layered") {
		for _, s := range scriptLines {
			if strings.Contains(string(s), "blend_normals") {
				blendNormalsFound = true
			}
			if strings.Contains(string(s), "invert_heights") {
				invertHeightsFound = true
			}
			if strings.Contains(string(s), "mask_gamma") {
				maskGammaFound = true
			}
			if strings.Contains(string(s), "paint_details") {
				paintDetailsFound = true
			}
		}

		if !blendNormalsFound || !invertHeightsFound || !maskGammaFound || !paintDetailsFound {
			return errors.New("Неполный список параметров для rendinst_perlin_layered")
		}
	}

	return nil
}

func (m *Material) TexCheck() map[error]struct{} {
	texLines := make([]Line, 0)
	var classLine Line
	for _, l := range m.lines {
		if l.Type() == Tex {
			texLines = append(texLines, l)
		}
		if l.Type() == Class {
			classLine = l
		}
	}

	errs := make(map[error]struct{})

	if strings.HasSuffix(string(classLine), "_layered\"") {
		if len(texLines) != 6 {
			errs[errors.New("Для _layered шейдера нужны 3 детейла(3 текстуры diffuse и 3 normal). Проверь верное количество текстур")] = struct {}{}
		}

		for i := range texLines {
			tl := strings.ReplaceAll(strings.Split(string(texLines[i]), ":")[0], " ", "")
			switch tl {
			case "tex0", "tex2", "tex3", "tex4", "tex5", "tex6":
			default:
				errs[errors.New("Назначен некорректный вид текстур. Проверь верно ли назначены текстуры diffuse(tex_d) и normal(tex_n) в соответствующих ячейках материала")] = struct{}{}
			}
		}
	}

	if strings.HasSuffix(string(classLine), "_simple\"") {
		if len(texLines) != 2 {
			errs[errors.New("Для _simple шейдера нужен 1 детейл(1 текстура diffuse и 1 normal). Проверь верное количество текстур")] = struct{}{}
		}

		for i := range texLines {
			tl := strings.ReplaceAll(strings.Split(string(texLines[i]), ":")[0], " ", "")
			switch tl {
			case "tex0", "tex2":
			default:
				errs[errors.New("Назначен некорректный вид текстур. Проверь верно ли назначены текстуры diffuse(tex_d) и normal(tex_n) в соответствующих ячейках материала")] = struct{}{}
			}
		}
	}

	return errs
}

func (m *Material) DiffuseNormalCheck() error {
	var class Line
	texs := make([]Line, 0)

	for _, l := range m.lines {
		if l.Type() == Class {
			class = l
		}
		if l.Type() == Tex {
			texs = append(texs, l)
		}
	}

	for i := range texs {
		tn := strings.ReplaceAll(strings.Replace(strings.Split(string(texs[i]), ":")[0], string(Tex), "", -1), " ", "")
		path := strings.Trim(strings.Trim(strings.Split(string(texs[i]), "=")[1], "t="), ":")
		name := filepath.Base(strings.Replace(path, filepath.Ext(path), "", -1))

		if strings.HasSuffix(string(class), "_layered\"") {
			switch tn {
			case "0", "3", "5":
				if !strings.HasSuffix(name, "tex_d") {
					return errors.New("Назначен некорректный вид текстур. Проверь верно ли назначены текстуры diffuse(tex_d) и normal(tex_n) в соответствующих ячейках материала")
				}
			case "2", "4", "6":
				if !strings.HasSuffix(name, "tex_n") {
					return errors.New("Назначен некорректный вид текстур. Проверь верно ли назначены текстуры diffuse(tex_d) и normal(tex_n) в соответствующих ячейках материала")
				}
			}
		}
		if strings.HasSuffix(string(class), "_simple\"") {
			switch tn {
			case "0":
				if !strings.HasSuffix(name, "tex_d") {
					return errors.New("Назначен некорректный вид текстур. Проверь верно ли назначены текстуры diffuse(tex_d) и normal(tex_n) в соответствующих ячейках материала")
				}
			case "2":
				if !strings.HasSuffix(name, "tex_n") {
					return errors.New("Назначен некорректный вид текстур. Проверь верно ли назначены текстуры diffuse(tex_d) и normal(tex_n) в соответствующих ячейках материала")
				}
			}
		}
	}

	return nil
}

func (m *Material) UsePaintingCheck() error {
	for i := range m.lines {
		if m.lines[i].Type() == Script {
			if strings.Contains(string(m.lines[i]), "use_painting") {
				str := strings.Trim(strings.Split(string(m.lines[i]), "=")[2], "\"")
				if str != "1" && str != "0" {
					return errors.New("Нужно проверить значение параметра use_painting. Он может быть только 0 или 1, любые другие значения являются багом")
				}
			}
		}
	}

	return nil
}

func (m *Material) EmptyClassCheck() error {
	var class, name string
	for i := range m.lines {
		if m.lines[i].Type() == Class {
			class = strings.Trim(strings.Split(string(m.lines[i]), "=")[1], "\"")
		}
		if m.lines[i].Type() == Name {
			name = strings.Trim(strings.Split(string(m.lines[i]), "=")[1], "\"")
		}
	}

	if class == "" && name != "autoNamedMat_0" {
		return errors.New("Данный материал в данном lod не назначен ни накакую геометрию. Возможно он является лишним и должен быть удален")
	}

	return nil
}
