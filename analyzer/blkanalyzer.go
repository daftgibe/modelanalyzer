package analyzer

import (
	"bufio"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
)

type BLKAnalyzer struct {
	BLKPath string
}

func (a *BLKAnalyzer) Analyze() map[string][]string {
	f, err := os.Open(a.BLKPath)
	if err != nil {
		log.Printf("cannot open blk \"%v\" file due error: %v", a.BLKPath, err)
		return nil
	}
	defer f.Close()

	materials := make([][]string, 0)
	materialLines := make([]string, 0)

	r := bufio.NewReader(f)
	for {
		l, _, err := r.ReadLine()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Printf("cannot read line from blk \"%v\" file due error: %v. stop process file", a.BLKPath, err)
			return nil
		}

		switch s := string(l); s {
		case string(MaterialStart):
			materialLines = make([]string, 0)
			materialLines = append(materialLines, s)
		case string(MaterialEnd):
			materialLines = append(materialLines, s)
			materials = append(materials, materialLines)
		case string(Empty):
			continue
		default:
			materialLines = append(materialLines, s)
		}
	}

	result := make(map[string][]string)
	for i := range materials {
		m := Material{
			fileName: strings.TrimSuffix(filepath.Base(a.BLKPath), filepath.Ext(filepath.Base(a.BLKPath))),
		}
		for ii := range materials[i] {
			m.lines = append(m.lines, Line(materials[i][ii]))
		}

		errs := m.Check()
		if len(errs) > 0 {
			for _, e := range errs {
				materialAsString := strings.Join(materials[i], "\n")
				if _, ok := result[materialAsString]; !ok {
					result[materialAsString] = []string{e.Error()}
				} else {
					result[materialAsString] = append(result[materialAsString], e.Error())
				}
			}
		}
	}

	return result
}
